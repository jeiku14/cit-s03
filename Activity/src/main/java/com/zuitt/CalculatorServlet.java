package com.zuitt;


import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 6106467607413134763L;
	
	public void init() throws ServletException{
		System.out.println("******************************************");
		System.out.println("CalculatorServlet has been initialized");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String op = req.getParameter("operation");
		double result = 0;
		
		if(op.equals("add")) {
			result = num1 + num2;
		}
		else if(op.equals("subtract")) {
			result = num1 - num2;
		}
		else if(op.equals("multiply")) {
			result = num1 * num2;
		}
		else if(op.equals("divide")) {
			result = (double) num1 / (double)num2;
		}
		
		PrintWriter out = res.getWriter();
		out.println("<p>The two numbers you provided are: " +num1+ ", " +num2+ "</p>");
		out.println("<p>The operation that you wanted is: " +op+ "</p>");
		out.println("<p>The result is: " +result+ "</p>");
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println("CalculatorServlet has been destroyed");
		System.out.println("******************************************");
	}
}
